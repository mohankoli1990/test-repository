
 function validation()
 {
    
	var fname=document.enq.fname.value;
	var name_exp=/^[A-Za-z\s]+$/;
	if(fname=='')
	{
		alert("First Name Field Should Not Be Empty!");
		document.enq.fname.focus();
		return false;
	}
	else if(!fname.match(name_exp))
	{
		alert("Invalid First Name field!");
		document.enq.fname.focus();
		return false;
	}
	
	var lname=document.enq.lname.value;
	if(lname=='')
	{
		alert("Last Name Field Should Not Be Empty!");
		document.enq.lname.focus();
		return false;
	}
	else if(!lname.match(name_exp))
	{
		alert("Invalid Last Name field!");
		document.enq.lname.focus();
		return false;
	}
	
	var email=document.enq.email.value;
	//var email_exp=/^[A-Za-z0-9\.-_\$]+@[A-Za-z]+\.[a-z]{2,4}$/;
	var email_exp=/^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
	if(email=='')
	{
		alert("Please Enter Email-Id!");
		document.enq.email.focus();
		return false;
	}
	else if(!email.match(email_exp))
	{
		alert("Invalid Email ID !");
		document.enq.email.focus();
		return false;
	}
	
	
	var phone=document.enq.phone.value;
	var phone_exp=/^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/;
	if(phone=='')
	{
		alert("Phone Field Should Not Be Empty!");
		document.enq.phone.focus();
		return false;
	}
	else if(!phone.match(phone_exp))
	{
		alert("Invalid Phone field!");
		document.enq.phone.focus();
		return false;
	}
	
	
	var message=document.enq.message.value;
	if(message=='')
	{
		alert("Query Field Should Not Be Empty!");
		document.enq.message.focus();
		return false;
	}
    return true;
 }
